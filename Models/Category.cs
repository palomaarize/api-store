using System.ComponentModel.DataAnnotations;

namespace Store.Models
{

    public class Category
    {

    [Key]

    public int Id {get; set;}

    [MaxLength(60, ErrorMessage = "Este campo deve conter entre no máximo 60 caracteres")]
    [MinLength(3, ErrorMessage = "Estecampo deve conter no mínimo 3 caracteres")]

    public string Title { get; set;}
    }
}